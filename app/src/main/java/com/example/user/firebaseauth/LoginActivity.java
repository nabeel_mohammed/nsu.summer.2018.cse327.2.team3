package com.example.user.firebaseauth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private Button buttonSignin;
    private EditText editTextMail;
    private EditText editTextPassword;
    private TextView textViewSignup;
    private ProgressDialog progressdialog;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        editTextMail=(EditText)findViewById(R.id.editTextMail);
        buttonSignin=(Button) findViewById(R.id.buttonLogIn);
        editTextPassword=(EditText)findViewById(R.id.editTextPassword);
        textViewSignup=(TextView) findViewById(R.id.textViewSignUp);
        progressdialog=new ProgressDialog(this);
        firebaseAuth=FirebaseAuth.getInstance();
        buttonSignin.setOnClickListener(this);
        textViewSignup.setOnClickListener(this);
    }

    private  void userLogin(){
        String email=editTextMail.getText().toString().trim();
        String passoword=editTextPassword.getText().toString().trim();
        if(TextUtils.isEmpty(email)){

            ///email is empty
            Toast.makeText(this, "Please enter your email", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(passoword)){
            ///email is empty
            Toast.makeText(this, "Please enter your password", Toast.LENGTH_SHORT).show();
            return;
        }
        progressdialog.setMessage("Registering user");
        progressdialog.show();
        firebaseAuth.createUserWithEmailAndPassword(email,passoword)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressdialog.dismiss();
                        if(task.isSuccessful()){

                            Toast.makeText(MainActivity.this,"register successful",Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(MainActivity.this,"could not register",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public void onClick(View view) {
        if(view==buttonSignin){
            userLogin();
        }
        if(view==textViewSignup){
            finish();
            startActivity(new Intent(this,MainActivity.class));
        }

    }
}
